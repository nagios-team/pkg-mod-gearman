#!/bin/sh

test_description="Check that the gearman-job-server starts"

. ./sharness.sh

test_expect_success "service status" "
  /usr/sbin/service mod-gearman-worker status
"

test_expect_success "process running" "
  pgrep -u nagios -f mod_gearman_worker
"

test_done
